import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../services/customer/customer.service';
import { PrinterService } from '../services/printer/printer.service';
import { OrdersService } from '../services/orders/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  response = { status: -1, message: "" };
  index:any;
  method = 'insert';
  view = false;
  viewPrint = false;
  arrayOrder:any = [];
  viewDetail = true;
  name:any;
  order_id:any;
  order_idCustomer:any;
  order_customerName:any;
  order_quantityCyan:any;
  order_quantityMagenta:any;
  order_quantityYellow:any;
  order_quantityBlack:any;
  arrayCustomer:any;
  selectCyan:any = '-1';
  selectMagenta:any = '-1';
  selectYellow:any = '-1';
  selectBlack:any = '-1';
  selectCustomer:any = '-1';
  cyanview = false;
  magentaview = false;
  yellowview = false;
  blackview = false;
  currentPage = 1;
  itemsPerPage =10;
  pageSize: number;
  constructor(private orderController: OrdersService,private customerController: CustomerService) { }

  ngOnInit() {
    this.getOrder()
  }

  getOrder(){  
    let customer = {method:'list'}
    this.orderController.registerOrder(customer)
    .then(data => {
      console.log(data)
      if(data != null){
        if(data['status']== 1){
          this.arrayOrder = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  addOrder(){
  if(this.order_idCustomer != '' && this.name != '' && this.selectCyan != '-1'  && this.selectMagenta != '-1'  && this.selectYellow != '-1'  && this.selectBlack != '-1'){
      let order:any;
      if(this.method === "update"){
        order = {order:{id_customer: this.order_idCustomer,customer_name:this.name,Cyan:{option:this.selectCyan,quantity:this.order_quantityCyan},Magenta:{option:this.selectMagenta,quantity:this.order_quantityMagenta},Yellow:{option:this.selectYellow,quantity:this.order_quantityYellow},Black:{option:this.selectBlack,quantity:this.order_quantityBlack}},id_order:this.order_id['$oid'].toString(),method: this.method}
      }else{
        order = {order:{id_customer: this.order_idCustomer,customer_name:this.name,Cyan:{option:this.selectCyan,quantity:this.order_quantityCyan},Magenta:{option:this.selectMagenta,quantity:this.order_quantityMagenta},Yellow:{option:this.selectYellow,quantity:this.order_quantityYellow},Black:{option:this.selectBlack,quantity:this.order_quantityBlack}},method: this.method}
      }
      this.orderController.registerOrder(order)
      .then(data => {
        if(data != null){
          console.log(data)
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            if(this.method == 'insert'){
              this.arrayOrder.push({id:data['id'],id_customer: this.order_idCustomer,customer_name:this.name,quantity_cyan:this.order_quantityCyan,quantity_magenta:this.order_quantityMagenta,quantity_yellow:this.order_quantityYellow,quantity_black:this.order_quantityBlack})
            }else{
              this.arrayOrder[this.index].id_customer = this.order_idCustomer 
              this.arrayOrder[this.index].customer_name = this.name
              this.arrayOrder[this.index].quantity_black = this.order_quantityBlack
              this.arrayOrder[this.index].quantity_cyan = this.order_quantityCyan
              this.arrayOrder[this.index].quantity_magenta = this.order_quantityMagenta
              this.arrayOrder[this.index].quantity_yellow = this.order_quantityYellow
            }
            this.cleanOrder()
            this.view = false;
            this.method = 'insert'
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  updateOrder(value){
    this.getCustomer()
    console.log(this.arrayOrder[value])
    this.view = true;
    this.index = value;
    this.order_idCustomer = this.arrayOrder[value].id_customer
    this.name = this.arrayOrder[value].customer_name
    this.selectCustomer = this.arrayOrder[value].id_customer
    this.order_quantityBlack = this.arrayOrder[value].quantity_black
    this.order_quantityCyan = this.arrayOrder[value].quantity_cyan
    this.order_quantityMagenta = this.arrayOrder[value].quantity_magenta
    this.order_quantityYellow = this.arrayOrder[value].quantity_yellow

    if(this.order_quantityBlack == "0"){
      this.selectBlack = "No"
      this.blackview = false
    }else{
      this.selectBlack = "Yes"
      this.blackview = true
    }

    if(this.order_quantityCyan == "0"){
      this.selectCyan = "No"
      this.cyanview = false
    }else{
      this.selectCyan = "Yes"
      this.cyanview = true
    }

    if(this.order_quantityMagenta == "0"){
      this.selectMagenta = "No"
      this.magentaview = false
    }else{
      this.selectMagenta = "Yes"
      this.magentaview = true
    }

    if(this.order_quantityYellow == "0"){
      this.selectYellow = "No"
      this.yellowview = false
    }else{
      this.selectYellow = "Yes"
      this.yellowview = true
    }

    this.method = 'update'
    this.order_id = this.arrayOrder[value].id
  }

  deleteOrder(value){
    let order = {id_order: this.arrayOrder[value]['id']['$oid'],method:'delete'}
    this.orderController.registerOrder(order)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayOrder.splice(value,1)
          }
          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  createOrder(value){
    this.view = value;
    this.getCustomer()
    if(this.view){
      this.cleanOrder()
    }
  }

  getCustomer(){  
    let customer = {method:'list'}
    this.customerController.registerCustomer(customer)
    .then(data => {
      if(data != null){
        console.log(data)
        if(data['status']== 1){
          this.arrayCustomer = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }
  
  cleanOrder(){
    this.selectBlack = '-1'
    this.selectCustomer = ''
    this.selectCyan = '-1'
    this.selectMagenta = '-1'
    this.selectYellow = '-1'
    this.order_idCustomer = ''
    this.cyanview = false;
    this.yellowview = false;
    this.magentaview = false;
    this.blackview = false;
    this.order_quantityBlack = ''
    this.order_quantityCyan = ''
    this.order_quantityMagenta = ''
    this.order_quantityYellow = ''
    this.method = 'insert'
  }


  selectOptionCustomer(value){
    
    for(let i=0;i<this.arrayCustomer.length;i++) {
      if(this.arrayCustomer[i].id.$oid === value){
        this.name = this.arrayCustomer[i].name
      }
    }
    this.order_idCustomer = value
  }

  selectOption(select,value){
    if(select == "1"){
      if(value == "Yes"){
          this.cyanview = true;
      }else{
        this.cyanview = false;
        this.order_quantityCyan = '0'
      }
    }else if(select == "2"){
      if(value == "Yes"){
          this.magentaview = true;
      }else{
        this.magentaview = false;
        this.order_quantityMagenta = '0'
      }
    }else if(select == "3"){
      if(value == "Yes"){
          this.yellowview = true;
      }else{
        this.yellowview = false;
        this.order_quantityYellow = '0'
      }
    }else if(select == "4"){
      if(value == "Yes"){
          this.blackview = true;
      }else{
        this.blackview = false;
        this.order_quantityBlack = '0'
      }
    }
  }

  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  deleteview(){
    this.view = false
  }
}

