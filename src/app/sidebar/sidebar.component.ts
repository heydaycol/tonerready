import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  menus=  JSON.parse(localStorage.getItem('optionsMenu'));
  public samplePagesCollapsed = true;
  
  constructor(private modalService:NgbModal,public router: Router) {
  }

  user:any;
  ngOnInit() {
    this.user = localStorage.getItem('user')
    this.user = JSON.parse(this.user)
  }

}
