import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";
@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http:HttpClient) { }

  registerOrder(order){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiOrder.php', order)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
