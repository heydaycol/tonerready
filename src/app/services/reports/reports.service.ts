import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http:HttpClient) { }

  configureHour(configure){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiConfigure.php', configure)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }

  // createServices(configure){
  //   return new Promise(
  //     resolve => {
  //       this.http.put(Constants.BASE_URL + 'apiConfigure.php', configure)
  //         .subscribe(
  //           data => resolve(data)
  //         )
  //     }
  //   )
  // }

  sendEmail(){
    return new Promise(
      resolve => {
        this.http.get(Constants.BASE_URL + 'apiEmail.php')
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
