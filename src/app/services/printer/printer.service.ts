import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";
@Injectable({
  providedIn: 'root'
})
export class PrinterService {

  constructor(private http:HttpClient) { }

  registerPrinter(printer){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiPrinter.php', printer)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
