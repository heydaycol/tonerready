import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";

@Injectable({
  providedIn: 'root'
})
export class TonerService {

  constructor(private http:HttpClient) { }

  registerToner(toner){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiToner.php', toner)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
