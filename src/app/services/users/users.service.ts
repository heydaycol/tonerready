import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) { }

  registerUser(user){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiUser.php', user)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
