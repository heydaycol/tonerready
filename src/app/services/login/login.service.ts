import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }


isLogin(){
  return new Promise(
    resolve => {
      this.http.get(Constants.BASE_URL + 'apiLogin.php')
        .subscribe(
          data => resolve(data)
        )
    }
  )
}


  login(user){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiLogin.php', user)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
