import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Constants } from "../constants";
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http:HttpClient) { }


  registerCustomer(customer){
    return new Promise(
      resolve => {
        this.http.post(Constants.BASE_URL + 'apiRegister.php', customer)
          .subscribe(
            data => resolve(data)
          )
      }
    )
  }
}
