import { Component, OnInit } from '@angular/core';
import{LoginService} from '../services/login/login.service';
import {  Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user={email:'', password:''}
  islogin=0;
  response={status:-1,message:"" };
  constructor(private LoginController:LoginService, private router:Router) { }

  ngOnInit() {      
//this.isLoginAt();
//this.islogin=1;
  }
// isLoginAt(){
//   this.LoginController.isLogin()
//   .then(data => {
//     console.log(data);

//     if(localStorage.getItem('user')!=null){
//       this.islogin=1;
//     }else{
//       this.islogin=0;
//     }
  
    
//   })
//   .catch(err => {
//     console.log(err);
//   });
// }

login(){
  if(this.user.email ===''){
    this.response.status=0;
    this.response.message='The email field is empty ';
    setTimeout(()=>{   
      this.response.status=-1;
      this.response.message="";
   }, 10000);
  }else if(this.user.password ===''){
    this.response.status=0;
    this.response.message='The password field is empty';
    setTimeout(()=>{   
    this.response.status=-1;
      this.response.message="";
   }, 10000);
  }else{
    this.LoginController.login(this.user)
    .then(data => {
      console.log(data);
      if(data['status']==1){
        this.islogin=1;
        localStorage.setItem('user',JSON.stringify(data['data']))
      }else{
          this.response.status=data['status'];
          this.response.message=data['message'];
        setTimeout(()=>{   
              this.response.status=-1;
              this.response.message="";
           }, 15000);
      }
    
      
    })
    .catch(err => {
      console.log(err);
    });
  }
  
}
 
  
}
