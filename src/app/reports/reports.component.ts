import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../services/reports/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  constructor(private reportController: ReportsService) { }
  response = { status: -1, message: "" };
  view = false;
  viewEmail = false;
  hour:any;
  arrayreport:any = [];
  arrayEmail:any = [];
  method:any;
  email:any;
  index:any;
  ngOnInit() {
    this.getReport()
  }

  getReport(){  
    let configure = {method:'list'}
    this.reportController.configureHour(configure)
    .then(data => {
      console.log(data)
      if(data != null){
        if(data['status']== 1){
          this.arrayreport = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }


  addReport(){
    console.log(this.hour)
    if(this.hour != ''){
      let printer:any;
      this.method = "update"
       printer = {hour:this.hour,method: this.method}
      this.reportController.configureHour(printer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
              this.arrayreport[0].hour = this.hour
            }
            this.hour = ''
            this.view = false;
        }
      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  updateReport(value){
    this.view = true;
    this.hour = this.arrayreport[value].hour
  }

  deleteview(){
    this.view = false
  }

  sendEmail(){
    this.reportController.sendEmail()
      .then(data => {
        if(data != null){
          if(data['status'] == 1){
            this.timeOut(data['status'],"Email Send")
          }else if(data['status'] == 0){
            this.timeOut(data['status'],"Problems with Mail Configuration")
          }else if(data['status'] == 2){
            this.timeOut(0,"Report Not Send")
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }

  getEmails(){
    console.log(this.arrayreport[0].send_email)
    this.arrayEmail = this.arrayreport[0].send_email
  }

  updateEmail(value){
    this.viewEmail= true
    this.email = this.arrayEmail[value]
    this.index = value
    this.method = "update_email"
  }

  deleteEmail(value){
    let array = this.arrayEmail
    array.splice(value,1)
    let customer:any;
    customer = {email: array,method:'delete'}
   this.reportController.configureHour(customer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            //this.arrayEmail.splice(value,1)
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  createEmail(value){
    this.viewEmail = value
    this.email = ''
    this.method = 'insert'
  }


  addEmail(){
    if(this.email != ''){
      let emails:any;
      if(this.method === "update_email"){
        let arrayEmail:any = this.arrayEmail
        arrayEmail[this.index] = this.email
        emails = {email:arrayEmail,method: this.method}
      }else{
        this.method = "insert"
        emails = {email:this.email,method: this.method}
      }
     
      this.reportController.configureHour(emails)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            if(this.method === "update_email"){
             this.arrayEmail[this.index] = this.email
            }else{
              this.arrayEmail.push(this.email)
            }
            }
            this.email = ''
            this.viewEmail = false;
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
  }
}
