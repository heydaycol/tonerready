import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TonersComponent } from './toners.component';

describe('TonersComponent', () => {
  let component: TonersComponent;
  let fixture: ComponentFixture<TonersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TonersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TonersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
