import { Component, OnInit, Input } from '@angular/core';
import { TonerService } from '../services/toner/toner.service';

@Component({
  selector: 'app-toners',
  templateUrl: './toners.component.html',
  styleUrls: ['./toners.component.scss']
})
export class TonersComponent implements OnInit {

  response = { status: -1, message: "" };
  toner = {
    id: '',
    id_printer: '',
    cyan:'',
    magenta:'',
    yellow: '',
    black:'',
    method: ''
  };
  cyan:any;
  magenta:any;
  yellow:any;
  black:any;
  id_printer:any;
  method:any = 'insert';
  viewPrint = false;
  view = false;
  
  @Input() arrayToner:any;
  @Input() filter:any;
  constructor(private tonerController: TonerService) { 
    let id:any= localStorage.getItem('id_printer')
    if(id != null){
    this.id_printer = id
    }
  }

  ngOnInit() {
    if(this.filter == null){
   //   this.getToner()
    }
  }

  getToner(){  
    let toner = {method:'list'}
    this.tonerController.registerToner(toner)
    .then(data => {
      if(data != null){
        if(data['status']== 1){
          this.arrayToner = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  addToner(){
    if(this.id_printer != '' && this.cyan != '' && this.magenta != '' && this.yellow != '' && this.black != ''){
      this.method = 'insert'
      let toner = {cyan: this.cyan,yellow:this.yellow,magenta:this.magenta,black:this.black,method:this.method,id_printer:this.id_printer}
      this.tonerController.registerToner(toner)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayToner[0].toners[0].percentage = this.cyan
            this.arrayToner[0].toners[1].percentage = this.magenta
            this.arrayToner[0].toners[2].percentage = this.yellow
            this.arrayToner[0].toners[3].percentage = this.black
          }
          this.cleanToner()
          this.view = false;
        }


      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  createToner(value){
    this.view = value;
  }

  updateToner(value){
    console.log(this.arrayToner[value])
    this.view = true
    this.cyan = this.arrayToner[value].toners[0].percentage
    this.magenta = this.arrayToner[value].toners[1].percentage
    this.yellow = this.arrayToner[value].toners[2].percentage
    this.black = this.arrayToner[value].toners[3].percentage
  }

  deleteToner(value){
    let toner = {id_user: this.arrayToner[value]['id']['$oid'],method:'delete'}
    this.tonerController.registerToner(toner)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayToner.splice(value,1)
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  cleanToner(){
    this.cyan = ''
    this.magenta = ''
    this.yellow = ''
    this.black = ''
  }

  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }
}
