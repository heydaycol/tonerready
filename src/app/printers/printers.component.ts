import { Component, OnInit,Input } from '@angular/core';
import { PrinterService } from '../services/printer/printer.service';
import { cleanSession } from 'selenium-webdriver/safari';
import { TonerService } from '../services/toner/toner.service';
import { OrdersService } from '../services/orders/orders.service';

@Component({
  selector: 'app-printers',
  templateUrl: './printers.component.html',
  styleUrls: ['./printers.component.scss']
})
export class PrintersComponent implements OnInit {
  response = { status: -1, message: "" };
  printer = {
    id: '',
    id_customer: '',
    serial:'',
    make:'',
    ip: '',
    note: '',
    method: ''
  };
  index:any;
  method = 'insert';
  view = false;
  viewPrint = false;
  viewDetail = true;
  notificationview = false;
  selectnotification = '-1'; 
  printer_idCustomer:any;
  printer_serial:any;
  printer_make:any;
  printer_ip:any;
  printer_note:any;
  printer_method:any;
  printer_model:any;
  printer_id:any;
  percentage_status:any;
  @Input() arrayPrinter:any = [];
  @Input() filter:any;
  arrayToner:any;
  arrayOrder:any;
  currentPage = 1;
  itemsPerPage =10;
  pageSize: number;
  constructor(private printerController: PrinterService,private tonerController: TonerService,private orderController: OrdersService) { 
    let id = localStorage.getItem('id_customer')
    if(id != null){
      this.printer_idCustomer = id
    }
  }

  ngOnInit() {
    if(this.filter == null){
      this.getPrinter()
    }
    this.getStock()
  }

  getPrinter(){  
    let customer = {method:'list'}
    this.printerController.registerPrinter(customer)
    .then(data => {
      if(data != null){
        if(data['status']== 1){
          this.arrayPrinter = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  createPrinter(value){
    this.view = value;
    if(this.view){
      this.cleanPrinter()
    }
  }



  addPrinter(){
    if(this.printer_idCustomer != '' && this.printer_serial != '' && this.printer_make != '' && this.printer_model != '' && this.printer_ip != '' && this.printer_note != '' && this.selectnotification != '-1' ){
      
      let printer:any;
      if(this.method === "update"){
       printer = {printer:{id_customer: this.printer_idCustomer,serial:this.printer_serial,make:this.printer_make,model:this.printer_model,ip:this.printer_ip,note:this.printer_note,notification:this.selectnotification,percentage:this.percentage_status},id_printer:this.printer_id['$oid'].toString(),method: this.method}
      }else{
       printer = {printer:{id_customer: this.printer_idCustomer,serial:this.printer_serial,make:this.printer_make,model:this.printer_model,ip:this.printer_ip,note:this.printer_note,notification:this.selectnotification,percentage:this.percentage_status},method: this.method}
      }
      this.printerController.registerPrinter(printer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            if(this.method == 'insert'){
              this.arrayPrinter.push({_id:data['id'],id_customer: this.printer_idCustomer,serial:this.printer_serial,make:this.printer_make,model:this.printer_model,ip:this.printer_ip,note:this.printer_note,notification:{send:this.selectnotification,percentage:this.percentage_status}})
            }else{
              this.arrayPrinter[this.index].serial = this.printer_serial
              this.arrayPrinter[this.index].make = this.printer_make
              this.arrayPrinter[this.index].model = this.printer_model
              this.arrayPrinter[this.index].ip = this.printer_ip
              this.arrayPrinter[this.index].note = this.printer_note
              this.arrayPrinter[this.index].notification.send = this.selectnotification
              this.arrayPrinter[this.index].notification.percentage = this.percentage_status
            }
            this.cleanPrinter()
            this.view = false;
            this.method = 'insert';
           }
        }
      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  updatePrinter(value){
    this.view = true;
    this.index = value;
    this.printer_make = this.arrayPrinter[value].make
    this.printer_serial = this.arrayPrinter[value].serial
    this.printer_ip = this.arrayPrinter[value].ip
    this.printer_model = this.arrayPrinter[value].model
    this.printer_note = this.arrayPrinter[value].note
    this.selectnotification = this.arrayPrinter[value].notification.send
    this.percentage_status = this.arrayPrinter[value].notification.percentage
    this.method = 'update'
    
    this.printer_id = this.arrayPrinter[value]._id
    this.printer_idCustomer = this.arrayPrinter[value].id_customer
    if(this.selectnotification == "1"){
      this.notificationview = true
    }else{
      this.notificationview = false  
    }
  }

  deletePrinter(value){
    let customer:any;
    customer = {id_user: this.arrayPrinter[value]['_id']['$oid'],method:'delete'}
   this.printerController.registerPrinter(customer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayPrinter.splice(value,1)
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  cleanPrinter(){
      this.printer_make = ''
      this.printer_serial = ''
      this.printer_ip = ''
      this.printer_model = ''
      this.printer_note = ''
      this.selectnotification = '-1'
      this.percentage_status = ''
      this.method = 'insert'
  }

  viewToner(value,id){
     let id_printer = id['_id']['$oid']
     if(value){
      this.getToner(value,id_printer)
     }else{
      this.viewPrint = value;
      this.viewDetail = !value;
     }
      
  }


  getToner(value,id){  
    let customer = {method:'filter',id:id}
    this.tonerController.registerToner(customer)
    .then(data => {
      if(data != null){
        if(data['status']== 1){
          this.arrayToner = data['data']
          this.viewPrint = value;
          this.viewDetail = !value;
          if(this.filter != null){
            this.filter = true;
          }
          localStorage.setItem('id_printer',id)
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  getStock(){
    let customer = {method:'stock',id: this.printer_idCustomer}
    this.orderController.registerOrder(customer)
    .then(data => {
      console.log(data)
      if(data != null){
        if(data['status']== 1){
          this.arrayOrder = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  selectOption(value){
    if(value == 1){
        this.notificationview = true;
    }else{
      this.notificationview = false;
      this.percentage_status = "0";
    }
  }
  deleteview(){
    this.view = false
  }
}
