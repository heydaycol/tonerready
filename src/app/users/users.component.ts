import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  response = { status: -1, message: "" };
  index:any;
  method = 'insert';
  view = false;
  viewPrint = false;
  arrayUser:any = [];
  user_name:any;
  user_email:any;
  user_password:any;
  user_rol:any = '-1';
  user_id:any;
  rol:any = '-1';
  currentPage = 1;
  itemsPerPage =10;
  pageSize: number;
  constructor(private userController: UsersService) { }

  ngOnInit() {
    this.getUser()
  }

  getUser(){  
    let user = {method:'list'}
    this.userController.registerUser(user)
    .then(data => {
      console.log(data)
      if(data != null){
        if(data['status']== 1){
          this.arrayUser = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  addUser(){
  if(this.user_name != '' && this.user_email != '' && this.user_password != '' && this.rol != '-1'){
      let user:any;
      if(this.method === "update"){
        user = {user:{name: this.user_name,email:this.user_email,password:this.user_password,rol:this.rol},id_user:this.user_id['$oid'],method: this.method}
      }else{
        user = {user:{name: this.user_name,email:this.user_email,password:this.user_password,rol:this.rol},method: this.method}
      }
      console.log(user)
      this.userController.registerUser(user)
      .then(data => {
        if(data != null){
          console.log(data)
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            if(this.method == 'insert'){

              this.arrayUser.push({id:data['id'],name: this.user_name,email:this.user_email,password:this.user_password,rol:this.rol})
            }else{
              this.arrayUser[this.index].name = this.user_name 
              this.arrayUser[this.index].email = this.user_email 
              this.arrayUser[this.index].rol = this.rol
              this.arrayUser[this.index].password = this.user_password 
            }
            this.cleanUser()
            this.view = false;
            this.method = 'insert'
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  updateUser(value){
    this.view = true;
    this.index = value;
    this.user_name = this.arrayUser[value].name
    this.user_email = this.arrayUser[value].email
    let rol = this.arrayUser[value].rol
    if(rol === "Administrator"){
      this.user_rol = "1"
    }else if(rol === "Customer"){
      this.user_rol = "2"
    }
    this.user_password = this.arrayUser[value].password
    this.method = 'update'
    this.user_id = this.arrayUser[value].id
  }

  deleteUser(value){
    let customer = {id_user: this.arrayUser[value]['id']['$oid'],method:'delete'}
    this.userController.registerUser(customer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayUser.splice(value,1)
          }
          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  createUser(value){
    this.view = value;
    if(this.view){
      this.cleanUser()
    }
  }

  cleanUser(){
    this.user_name = ' '
    this.user_email = ''
    this.user_rol = '-1'
    this.user_password = ''
    this.method = 'insert'
  }

  selectOption(value){
    if(value == 1){
      this.rol = 'Administrator'
    }else if(value == 2){
      this.rol = "Customer"
    }
    console.log(this.rol)
  }

  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }
  deleteview(){
    this.view = false
  }
}
