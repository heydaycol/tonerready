import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../services/customer/customer.service';
import { PrinterService } from '../services/printer/printer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  response = { status: -1, message: "" };
  customers = {
    id:'0',
    name: '',
    address:'',
    email:'',
    note: '',
  };
  index:any;
  method = 'insert';
  view = false;
  notificationview = false;
  viewPrint = false;
  arrayCustomer:any = [];
  viewDetail = true;
  selectnotification = '-1'; 
  customer_name:any;
  customer_email:any;
  customer_note:any;
  customer_address:any;
  customer_id:any;
  percentage_status:any;
  arrayPrinter:any;
  filter:any;
  currentPage = 1;
  itemsPerPage =10;
  pageSize: number;
  
  constructor(private customerController: CustomerService,private printerController: PrinterService) { }

  ngOnInit() {
    this.getCustomer()
  }

  getCustomer(){  
    let customer = {method:'list'}
    this.customerController.registerCustomer(customer)
    .then(data => {
      if(data != null){
        if(data['status']== 1){
          this.arrayCustomer = data['data']
        }
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  addCustomer(){

    if(this.customer_name != '' && this.customer_address != '' && this.customer_email != '' && this.customer_note != ''  && this.selectnotification != '-1'){
      let customer:any;
      if(this.method === "update"){
        customer = {customer:{name: this.customer_name,address:this.customer_address,email:this.customer_email,note:this.customer_note,notification:this.selectnotification,percentage:this.percentage_status},id_customer:this.customer_id['$oid'].toString(),method: this.method}
      }else{
        customer = {customer:{name: this.customer_name,address:this.customer_address,email:this.customer_email,note:this.customer_note,notification:this.selectnotification,percentage:this.percentage_status},method: this.method}
      }
      this.customerController.registerCustomer(customer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            if(this.method == 'insert'){
              this.arrayCustomer.push({id:data['id'],name: this.customer_name,address:this.customer_address,email:this.customer_email,note:this.customer_note,notification:{send:this.selectnotification,percentage:this.percentage_status}})
            }else{
              this.arrayCustomer[this.index].address = this.customer_address 
              this.arrayCustomer[this.index].email = this.customer_email 
              this.arrayCustomer[this.index].name = this.customer_name
              this.arrayCustomer[this.index].note = this.customer_note
              this.arrayCustomer[this.index].notification.send = this.selectnotification
              this.arrayCustomer[this.index].notification.percentage = this.percentage_status
            }
            this.cleanCustomer()
            this.view = false;
            this.method = 'insert'
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
    }else{
      this.timeOut(0,'Campos Vacios')
    }
  }

  updateCustomer(value){
    console.log(this.arrayCustomer[value])
    this.view = true;
    this.index = value;
    this.customer_address = this.arrayCustomer[value].address
    this.customer_email = this.arrayCustomer[value].email
    this.customer_name = this.arrayCustomer[value].name
    this.customer_note = this.arrayCustomer[value].note
    this.selectnotification = this.arrayCustomer[value].notification.send
    this.percentage_status = this.arrayCustomer[value].notification.percentage
    this.method = 'update'
    this.customer_id = this.arrayCustomer[value].id
    if(this.selectnotification == "1"){
      this.notificationview = true
    }else{
      this.notificationview = false  
    }
  }

  deleteCustomer(value){
    let customer = {id_user: this.arrayCustomer[value]['id']['$oid'],method:'delete'}
    this.customerController.registerCustomer(customer)
      .then(data => {
        if(data != null){
          this.timeOut(data['status'],data['message'])
          if(data['status']== 1){
            this.arrayCustomer.splice(value,1)
          }
          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  createCustomer(value){
    this.view = value;
    if(this.view){
      this.cleanCustomer()
    }
  }
  viewPrinter(value,id){
    if(value){
      this.getPrinter(value,id['$oid'])
    }else{
      this.viewPrint = value;
      this.viewDetail = !value;
    }
   
  }

  getPrinter(value,id){
    let customer = {method:'filter',id: id}
    this.printerController.registerPrinter(customer)
    .then(data => {
      if(data != null){
        if(data['status']== 1){
          this.arrayPrinter = data['data']
          this.filter = true;
          this.viewPrint = value;
          this.viewDetail = !value;
        }else{
          this.arrayPrinter = []
          this.filter = true;
          this.viewPrint = value;
          this.viewDetail = !value;
        }
        localStorage.setItem('id_customer',id)
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  cleanCustomer(){
    this.customer_name = ' '
    this.customer_address = ''
    this.customer_note = ''
    this.customer_email = ''
    this.selectnotification = '-1'
    this.percentage_status = ''
    this.method = 'insert'
  }
  timeOut(status, message) {
    this.response = { status: status, message: message };
    setTimeout(() => {
      this.response.status = -1;
      this.response.message = "";
    }, 4000);
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }


  selectOption(value){
      if(value == 1){
          this.notificationview = true;
      }else{
        this.notificationview = false;
        this.percentage_status = "0";
      }
    }
    deleteview(){
      this.view = false
    }
}
